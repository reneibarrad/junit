import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.ArrayList;
import java.util.List;

@RunWith(value = Parameterized.class)
public class ParametherTest {

    @Parameterized.Parameters
    public  Iterable<Object[]> getData (){
        List<Object[]> objects = new ArrayList<>();
        objects.add(new Object[]{1,4,5});
        return  objects;
    }

    private  int a,b,exp;
    public void  calculParametrosTest(int a, int b,int exp){
        this.a=a;
        this.b=b;
        this.exp=exp;
    }
}
